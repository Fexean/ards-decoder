# ARDS Decoder

Action Replay DS code decoder.  

### Example Output
    94000130 FDFF0000       IF (*(u16*)4000130h & 0200h == 0000h)
    B21C4D28 00000000           ofs = *(u32*)(21C4D28h)
    B0000004 00000000           ofs = *(u32*)(0000004h+ofs)
    D9000000 00112F58           datareg = *(u32*)(00112F58h+ofs)
    C0000000 0000000C           FOR (i = 0 ; i <= 0000000Ch)
    DC000000 00000004               ofs += 00000004h
    D6000000 0002461C               *(u32*)(0002461Ch+ofs) = datareg, ofs += 4
    D1000000 00000000           NEXT i

    C0000000 0000000A           FOR (i = 0 ; i <= 0000000Ah)
    D6000000 0002461C               *(u32*)(0002461Ch+ofs) = datareg, ofs += 4
    D2000000 00000000           ofs = 0; datareg = 0; ENDIF; NEXT i


# Usage
`ar_decoder file [-ob]`

Optional parameters:  
o = omit raw code from output  
b = print data bytes (E command type)  

# Compiling
You need to have [Nim](https://nim-lang.org/) installed.  
Then run `nim c ar_decoder.nim`

