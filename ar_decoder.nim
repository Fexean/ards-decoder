#? replace(sub = "\t", by = "  ")
import strutils, streams, os





# Replaces parameters in line with ones from code: 0XXXXXXX AABBCCDD
proc replaceLineParams(line : string, code : string) : string =
	let rep1 = ("XXXXXXX", code[1..7])
	let rep2 = ("AA", code[9..10])
	let rep3 = ("BB", code[11..12])
	let rep4 = ("CC", code[13..14])
	let rep5 = ("DD", code[15..16])
	result = line.multiReplace(rep1, rep2, rep3, rep4, rep5)



proc parse_code_0(code : string) : string =
	replaceLineParams("*(u32*)(XXXXXXXh+ofs) = AABBCCDDh", code);
	
proc parse_code_1(code : string) : string  =
	replaceLineParams("*(u16*)(XXXXXXXh+ofs) = CCDDh", code);
	
proc parse_code_2(code : string) : string  =
	replaceLineParams("*(u8*)(XXXXXXXh+ofs) = DDh", code);



proc print_cond1(code: string, cmp : string) : string =
	let dst = parseHexInt(code[1..7])
	if dst == 0:
		result = format("IF ( *(u32*)ofs $2 $1h)", code[9..16], cmp)
	else:
		result = format("IF ( *(u32*)$3h $2 $1h)", code[9..16], cmp, code[1..7])

proc parse_code_3(code : string) : string = 
	print_cond1(code, ">")

proc parse_code_4(code : string) : string = 
	print_cond1(code, "<")

proc parse_code_5(code : string) : string = 
	print_cond1(code, "==")

proc parse_code_6(code : string) : string = 
	print_cond1(code, "!=")
	


proc print_cond2(code, cmp : string) : string = 
	let dst = parseHexInt(code[1..7])
	let notZZZZ = uint16(not parseHexInt(code[9..^5]))
	if dst == 0:
		result = format("IF (*(u16*)ofs & $3h $2 $1h)", code[13..16], cmp, toHex(notZZZZ))
	else:
		result = format("IF (*(u16*)$3h & $4h $2 $1h)", code[13..16], cmp, code[1..7], toHex(notZZZZ))
	
	if notZZZZ == 0xFFFF:	# "& 0xffff" is not needed
		result = result.replace("& FFFFh ", "")


proc parse_code_7(code : string) : string = 
	print_cond2(code, ">")

proc parse_code_8(code : string) : string = 
	print_cond2(code, "<")

proc parse_code_9(code : string) : string = 
	print_cond2(code, "==")

proc parse_code_A(code : string) : string = 
	print_cond2(code, "!=")

proc parse_code_B(code : string) : string =
	replaceLineParams("ofs = *(u32*)(XXXXXXXh+ofs)", code);

proc parse_code_C(code : string) : string =
	replaceLineParams("FOR (i = 0 ; i <= AABBCCDDh)", code)

proc parse_code_C4(code : string) : string =
	"ofs = address of this line"
	
proc parse_code_C5(code : string) : string =
	"counter++"
	
proc parse_code_C6(code : string) : string =
	replaceLineParams("*(u32*)AABBCCDDh = ofs", code)

proc parse_code_D0(code : string) : string =
	"ENDIF"
	
proc parse_code_D1(code : string) : string =
	"NEXT i"
	
proc parse_code_D2(code : string) : string =
	"ofs = 0; datareg = 0; ENDIF; NEXT i"

proc parse_code_D3(code : string) : string =
	replaceLineParams("ofs = AABBCCDDh", code)
	
proc parse_code_D4(code : string) : string =
	replaceLineParams("datareg += AABBCCDDh", code)
	
proc parse_code_D5(code : string) : string =
	replaceLineParams("datareg = AABBCCDDh", code)
	
proc parse_code_D6(code : string) : string =
	replaceLineParams("*(u32*)(AABBCCDDh+ofs) = datareg, ofs += 4", code)
	
proc parse_code_D7(code : string) : string =
	replaceLineParams("*(u16*)(AABBCCDDh+ofs) = datareg, ofs += 2", code)
	
proc parse_code_D8(code : string) : string =
	replaceLineParams("*(u8*)(AABBCCDDh+ofs) = datareg, ofs += 1", code)

proc parse_code_D9(code : string) : string =
	replaceLineParams("datareg = *(u32*)(AABBCCDDh+ofs)", code)

proc parse_code_DA(code : string) : string =
	replaceLineParams("datareg = *(u16*)(AABBCCDDh+ofs)", code)
	
proc parse_code_DB(code : string) : string =
	replaceLineParams("datareg = *(u8*)(AABBCCDDh+ofs)", code)

proc parse_code_DC(code : string) : string =
	replaceLineParams("ofs += AABBCCDDh", code)

proc parse_code_F(code : string) : string =
	replaceLineParams("memcpy(XXXXXXXh, ofs, AABBCCDDh)", code)




type
	Decoder = object
		# Current indentation amount
		indent: int
		
		# True when a newline should be placed after the current line
		emitNewline: bool
		
		# ofs is deleted from the pseudocode if it hasn't been set
		deleteOfs: bool
		
		# Skip printing each line of data that gets written by E code type
		skipDataBytes: bool
		
		# Print raw code alongside pseudocode
		printOriginal: bool




proc print_code(state : var Decoder, strm : Stream) =
	var newIndent = state.indent
	var ofsModified = false
	var pseudocode : string
	let code = strm.readLine()
	case code[0]
	of '0':
		pseudocode = parse_code_0(code)
	of '1':
		pseudocode = parse_code_1(code)
	of '2':
		pseudocode = parse_code_2(code)
	of '3':
		pseudocode = parse_code_3(code)
		inc(newIndent)
	of '4':
		pseudocode = parse_code_4(code)
		inc(newIndent)
	of '5':
		pseudocode = parse_code_5(code)
		inc(newIndent)
	of '6':
		pseudocode = parse_code_6(code)
		inc(newIndent)
	of '7':
		pseudocode = parse_code_7(code)
		inc(newIndent)
	of '8':
		pseudocode = parse_code_8(code)
		inc(newIndent)
	of '9':
		pseudocode = parse_code_9(code)
		inc(newIndent)
	of 'A':
		pseudocode = parse_code_A(code)
		inc(newIndent)
	of 'B':
		pseudocode = parse_code_B(code)
		ofsModified = true
	of 'E':
		let linecount = int(int(7+parseHexInt(code[9..16]))/8)
		if not state.skipDataBytes:
			if state.printOriginal:
				write(stdout, code)
				write(stdout, "\t")
			echo replaceLineParams("Write following to XXXXXXXh+ofs(AABBCCDDh bytes): ", code).indent(state.indent*4)
			echo "{"
		else:
			pseudocode = replaceLineParams("Write AABBCCDDh bytes to XXXXXXXh+ofs", code)
		
		for i in 1..linecount:
			if state.skipDataBytes:
				discard strm.readLine()
			else:
				echo strm.readLine()
		
		if not state.skipDataBytes:
			echo "}"
			return
		
	of 'F':
		pseudocode = parse_code_F(code)
	else:
		case code[0..1]
		of "C0":
			pseudocode = parse_code_C(code)
			inc(newIndent)
			ofsModified = true
		of "C4":
			pseudocode = parse_code_C4(code)
			ofsModified = true
		of "C5":
			pseudocode = parse_code_C5(code)
		of "C6":
			pseudocode = parse_code_C6(code)
		of "D0":
			pseudocode = parse_code_D0(code)
			state.emitNewline = true	
			dec(state.indent)
			dec(newIndent)
		of "D1":
			pseudocode = parse_code_D1(code)
			state.emitNewline = true
			dec(state.indent)
			dec(newIndent)
		of "D2":
			pseudocode = parse_code_D2(code)
			state.emitNewline = true
			dec(state.indent)
			newIndent = 0
			state.deleteOfs = true
		of "D3":
			pseudocode = parse_code_D3(code)
			if code == "D3000000 00000000":		# "ofs = 0h"
				ofsModified = false
				state.deleteOfs = true
			else:
				ofsModified = true
		of "D4":
			pseudocode = parse_code_D4(code)
		of "D5":
			pseudocode = parse_code_D5(code)
		of "D6":
			pseudocode = parse_code_D6(code)
		of "D7":
			pseudocode = parse_code_D7(code)
		of "D8":
			pseudocode = parse_code_D8(code)
		of "D9":
			pseudocode = parse_code_D9(code)
		of "DA":
			pseudocode = parse_code_DA(code)
		of "DB":
			pseudocode = parse_code_DB(code)
		of "DC":
			pseudocode = parse_code_DC(code)
			ofsModified = true
		else:
			pseudocode = "ILLEGAL CODE"
	
	if state.indent < 0:
		state.indent = 0
		newIndent = 0

	if state.printOriginal:
		write(stdout, code)
		write(stdout, "\t")
	
	if state.deleteOfs:
		pseudocode = pseudocode.replace("+ofs","")
		if ofsModified:
			state.deleteOfs = false
	
	echo pseudocode.indent(state.indent*4);
	
	if newIndent != state.indent:
		state.indent = newIndent

	if state.emitNewline == true:
		state.emitNewline = false
		echo ""
	


proc decodeStream(state: var Decoder, strm : Stream) =
	state.emitNewline = false
	state.indent = 0
	state.deleteOfs = true
	
	while(not strm.atEnd()):
		print_code(state, strm)











var argv = commandLineParams()
if len(argv) < 1 :
	quit("USAGE: ar_decoder codefile [-ob]\n\nOptions:\no = don't print raw code next to pseudocode\nb = print data bytes")


var fstrm = openFileStream(argv[0])
var strm = newStringStream("")

var line = ""
while fstrm.readLine(line):
	let codes = line.split()
	for i in countup(0, high(codes)-1, 2):
		strm.writeLine(capitalizeAscii(codes[i]), " ", capitalizeAscii(codes[i+1]))
fstrm.close()

var state : Decoder


if(len(argv) == 2):
	state.skipDataBytes = not argv[1].contains('b')
	state.printOriginal = not argv[1].contains('o')
else:
	state.skipDataBytes = true
	state.printOriginal = true

strm.setPosition(0)
state.decodeStream(strm)
strm.close()